# LabeledFacesInTheWild

  <div align="center">
  <img src="https://cloud.githubusercontent.com/assets/5456665/20069800/4010bd08-a548-11e6-9678-a0cbfc8c93a3.png" height="80" width=auto title="Male">
  <img src="https://cloud.githubusercontent.com/assets/5456665/20069845/6235b118-a548-11e6-98a7-8b478c96facc.png" height="80" width=auto title="Female">
  <img src="https://cloud.githubusercontent.com/assets/5456665/20069806/406b241e-a548-11e6-98b5-abba1c9037c3.png" height="80" width=auto title="Male">
  <img src="https://cloud.githubusercontent.com/assets/5456665/20069844/6214b5e4-a548-11e6-8990-6bb1e749175a.png" height="80" width=auto title="Female">
  <img src="https://cloud.githubusercontent.com/assets/5456665/20069804/40158194-a548-11e6-99ed-560f81aca3ec.png" height="80" width=auto title="Male">
  <img src="https://cloud.githubusercontent.com/assets/5456665/20069846/624b8f92-a548-11e6-80f9-000fbcf2d1ec.png" height="80" width=auto title="Female">
  <img src="https://cloud.githubusercontent.com/assets/5456665/20069805/401928bc-a548-11e6-9f7b-ce7f0c52e69c.png" height="80" width=auto title="Male">
  <img src="https://cloud.githubusercontent.com/assets/5456665/20069842/620fca2a-a548-11e6-87e9-786becb8679d.png" height="80" width=auto title="Female">
  </div>

 - LFW-Face-Grey is cropped face grey-scaled image of LFW dataset. It contains 13233 images. Download it from [here](https://github.com/MinhasKamal/LabeledFacesInTheWild-LFW/blob/master/LFW-Face-Grey.rar?raw=true).
 - LFW-Gender-Face-Grey is also cropped face image. Here images are randomly selected and stored into five folders. In each folder images are further seperated into two folders *male* & *female*. Download all of them from [here](https://minhaskamal.github.io/DownGit/#/home?url=https:%2F%2Fgithub.com%2FMinhasKamal%2FLabeledFacesInTheWild-LFW%2Ftree%2Fmaster%2FLFW-Gender-Face-Grey).

### Source 
http://vis-www.cs.umass.edu/lfw/
